﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EatCalculator
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        public string RecipeTxB
        {
            get { return RecipeName.Text; }
            set { RecipeName.Text = value; }
        }

        /*public string RecipeType
        {
            get { return }
        }*/

        public void GetType()
        {
            bool soap = Convert.ToBoolean(this.typeSoap.CheckAlign);
            bool garnish = Convert.ToBoolean(this.typeGarnish.CheckAlign);
            bool dessert = Convert.ToBoolean(this.typeDessert.CheckAlign);
            bool snack = Convert.ToBoolean(this.typeSnack.CheckAlign);

            switch (soap)
            {
                case true:
                    break;

                default:
                    break;
            }

            switch (garnish)
            {
                case true:
                    break;

                default:
                    break;
            }

            switch (dessert)
            {
                case true:
                    break;

                default:
                    break;
            }

            switch (snack)
            {
                case true:
                    break;

                default:
                    break;
            }
        }

        private void Create_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();
            string k = Convert.ToString(RecipesList.SelectedItem);
            var checkedButton = groupBox1.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(r => r.Checked);
            string t = checkedButton.Text;
            string n = RecipeText.Text;
            string p = RecipePic.Text;

            form.CreateRecipe(k, t, n, p);
        }

        private void RecipeText_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            string filename = openFileDialog1.FileName;
            openFileDialog1.DefaultExt = ".txt";
            openFileDialog1.Filter = "Текстовые файлы (*.txt)|*.txt|Все файлы (*.*)|*.*";
            openFileDialog1.InitialDirectory = "AppDomain.CurrentDomain.BaseDirectory";
        }

        private void RecipePic_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
            string filename = openFileDialog1.FileName;
            openFileDialog2.DefaultExt = ".txt";
            openFileDialog2.Title = "Выберите изображение..";
            openFileDialog2.Filter = "Изображение PNG (*.png)|*.png|Все файлы (*.*)|*.*";
            openFileDialog2.InitialDirectory = "AppDomain.CurrentDomain.BaseDirectory";
            openFileDialog2.AddExtension = true;
            openFileDialog2.CheckFileExists = true;
            openFileDialog2.CheckPathExists = true;
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            RecipeText.Text = openFileDialog1.FileName;
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {
            RecipePic.Text = openFileDialog2.FileName;
        }

        public string DialogRecipeFile
        {
            get { return openFileDialog1.FileName; }
            set { openFileDialog1.FileName = value; }
        }

        public string DialogPicFile
        {
            get { return openFileDialog2.FileName; }
            set { openFileDialog2.FileName = value; }
        }
    }
}
