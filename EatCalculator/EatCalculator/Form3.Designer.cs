﻿namespace EatCalculator
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.RecipeName = new System.Windows.Forms.TextBox();
            this.EnterInfo = new System.Windows.Forms.TextBox();
            this.SetType = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.typeSnack = new System.Windows.Forms.RadioButton();
            this.typeSoap = new System.Windows.Forms.RadioButton();
            this.typeGarnish = new System.Windows.Forms.RadioButton();
            this.typeDessert = new System.Windows.Forms.RadioButton();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.RecipesList = new System.Windows.Forms.ListBox();
            this.Create = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.RecipeText = new System.Windows.Forms.TextBox();
            this.RecipePic = new System.Windows.Forms.TextBox();
            this.programBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.programBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.openFileDialog2 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource1)).BeginInit();
            this.SuspendLayout();
            // 
            // RecipeName
            // 
            this.RecipeName.Font = new System.Drawing.Font("Microsoft YaHei UI Light", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipeName.Location = new System.Drawing.Point(153, 50);
            this.RecipeName.Multiline = true;
            this.RecipeName.Name = "RecipeName";
            this.RecipeName.Size = new System.Drawing.Size(130, 27);
            this.RecipeName.TabIndex = 0;
            this.RecipeName.Text = "Мое блюдо";
            // 
            // EnterInfo
            // 
            this.EnterInfo.BackColor = System.Drawing.SystemColors.Control;
            this.EnterInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EnterInfo.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.EnterInfo.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EnterInfo.Location = new System.Drawing.Point(125, 12);
            this.EnterInfo.Multiline = true;
            this.EnterInfo.Name = "EnterInfo";
            this.EnterInfo.Size = new System.Drawing.Size(197, 31);
            this.EnterInfo.TabIndex = 1;
            this.EnterInfo.Text = "Введите название:";
            // 
            // SetType
            // 
            this.SetType.BackColor = System.Drawing.SystemColors.Control;
            this.SetType.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SetType.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.SetType.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SetType.Location = new System.Drawing.Point(115, 168);
            this.SetType.Multiline = true;
            this.SetType.Name = "SetType";
            this.SetType.Size = new System.Drawing.Size(183, 31);
            this.SetType.TabIndex = 3;
            this.SetType.Text = "Выберите тип:";
            this.SetType.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.typeSnack);
            this.groupBox1.Controls.Add(this.typeSoap);
            this.groupBox1.Controls.Add(this.typeGarnish);
            this.groupBox1.Controls.Add(this.typeDessert);
            this.groupBox1.Location = new System.Drawing.Point(103, 198);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(210, 81);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // typeSnack
            // 
            this.typeSnack.AutoSize = true;
            this.typeSnack.Location = new System.Drawing.Point(112, 42);
            this.typeSnack.Name = "typeSnack";
            this.typeSnack.Size = new System.Drawing.Size(67, 17);
            this.typeSnack.TabIndex = 7;
            this.typeSnack.TabStop = true;
            this.typeSnack.Text = "Закуски";
            this.typeSnack.UseVisualStyleBackColor = true;
            // 
            // typeSoap
            // 
            this.typeSoap.AutoSize = true;
            this.typeSoap.Location = new System.Drawing.Point(13, 19);
            this.typeSoap.Name = "typeSoap";
            this.typeSoap.Size = new System.Drawing.Size(51, 17);
            this.typeSoap.TabIndex = 8;
            this.typeSoap.TabStop = true;
            this.typeSoap.Text = "Супы";
            this.typeSoap.UseVisualStyleBackColor = true;
            // 
            // typeGarnish
            // 
            this.typeGarnish.AutoSize = true;
            this.typeGarnish.Location = new System.Drawing.Point(13, 42);
            this.typeGarnish.Name = "typeGarnish";
            this.typeGarnish.Size = new System.Drawing.Size(69, 17);
            this.typeGarnish.TabIndex = 6;
            this.typeGarnish.TabStop = true;
            this.typeGarnish.Text = "Гарниры";
            this.typeGarnish.UseVisualStyleBackColor = true;
            // 
            // typeDessert
            // 
            this.typeDessert.AutoSize = true;
            this.typeDessert.Location = new System.Drawing.Point(112, 19);
            this.typeDessert.Name = "typeDessert";
            this.typeDessert.Size = new System.Drawing.Size(71, 17);
            this.typeDessert.TabIndex = 5;
            this.typeDessert.TabStop = true;
            this.typeDessert.Text = "Десерты";
            this.typeDessert.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Control;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox2.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox2.Location = new System.Drawing.Point(125, 82);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(183, 31);
            this.textBox2.TabIndex = 5;
            this.textBox2.Text = "Выберите кухню:";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // RecipesList
            // 
            this.RecipesList.Font = new System.Drawing.Font("Algerian", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipesList.FormattingEnabled = true;
            this.RecipesList.ItemHeight = 18;
            this.RecipesList.Location = new System.Drawing.Point(152, 119);
            this.RecipesList.Name = "RecipesList";
            this.RecipesList.Size = new System.Drawing.Size(120, 22);
            this.RecipesList.TabIndex = 6;
            // 
            // Create
            // 
            this.Create.Font = new System.Drawing.Font("Microsoft YaHei Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Create.Location = new System.Drawing.Point(170, 366);
            this.Create.Name = "Create";
            this.Create.Size = new System.Drawing.Size(92, 33);
            this.Create.TabIndex = 7;
            this.Create.Text = "Добавить";
            this.Create.UseVisualStyleBackColor = true;
            this.Create.Click += new System.EventHandler(this.Create_Click);
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Control;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox1.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(115, 285);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(183, 31);
            this.textBox1.TabIndex = 8;
            this.textBox1.Text = "Выберите файл";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // RecipeText
            // 
            this.RecipeText.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipeText.Location = new System.Drawing.Point(103, 322);
            this.RecipeText.Name = "RecipeText";
            this.RecipeText.Size = new System.Drawing.Size(100, 21);
            this.RecipeText.TabIndex = 11;
            this.RecipeText.Text = "Рецепт";
            this.RecipeText.Click += new System.EventHandler(this.RecipeText_Click);
            // 
            // RecipePic
            // 
            this.RecipePic.Font = new System.Drawing.Font("Microsoft YaHei UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipePic.Location = new System.Drawing.Point(222, 322);
            this.RecipePic.Name = "RecipePic";
            this.RecipePic.Size = new System.Drawing.Size(100, 21);
            this.RecipePic.TabIndex = 12;
            this.RecipePic.Text = "Фото";
            this.RecipePic.Click += new System.EventHandler(this.RecipePic_Click);
            // 
            // programBindingSource
            // 
            this.programBindingSource.DataSource = typeof(EatCalculator.Program);
            // 
            // programBindingSource1
            // 
            this.programBindingSource1.DataSource = typeof(EatCalculator.Program);
            // 
            // openFileDialog2
            // 
            this.openFileDialog2.FileName = "openFileDialog2";
            this.openFileDialog2.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog2_FileOk);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 411);
            this.Controls.Add(this.RecipePic);
            this.Controls.Add(this.RecipeText);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.Create);
            this.Controls.Add(this.RecipesList);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.SetType);
            this.Controls.Add(this.EnterInfo);
            this.Controls.Add(this.RecipeName);
            this.Name = "Form3";
            this.Text = "Form3";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox RecipeName;
        private System.Windows.Forms.TextBox EnterInfo;
        private System.Windows.Forms.TextBox SetType;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton typeSnack;
        private System.Windows.Forms.RadioButton typeSoap;
        private System.Windows.Forms.RadioButton typeGarnish;
        private System.Windows.Forms.RadioButton typeDessert;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.ListBox RecipesList;
        private System.Windows.Forms.BindingSource programBindingSource;
        private System.Windows.Forms.Button Create;
        private System.Windows.Forms.BindingSource programBindingSource1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.TextBox RecipeText;
        private System.Windows.Forms.TextBox RecipePic;
        private System.Windows.Forms.OpenFileDialog openFileDialog2;
    }
}