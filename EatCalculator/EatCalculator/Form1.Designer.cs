﻿namespace EatCalculator
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Отбивная");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Итальянская", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Баурсак");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Казы");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Казахская", new System.Windows.Forms.TreeNode[] {
            treeNode3,
            treeNode4});
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Китайская");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Мексиканская");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Польская");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Румынская");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Салат с хреном");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Винегрет");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Драники");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Закуски", new System.Windows.Forms.TreeNode[] {
            treeNode10,
            treeNode11,
            treeNode12});
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Борщ");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Окрошка");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Щи");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Ботвинья");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Супы", new System.Windows.Forms.TreeNode[] {
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Вареники");
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Пельмени");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Беф строганов");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Биточки");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Гарниры", new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode20,
            treeNode21,
            treeNode22});
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Хворост");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Десерты", new System.Windows.Forms.TreeNode[] {
            treeNode24});
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Русская", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode18,
            treeNode23,
            treeNode25});
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Лагман");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Узбекская", new System.Windows.Forms.TreeNode[] {
            treeNode27});
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("Биточки");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("Украинская", new System.Windows.Forms.TreeNode[] {
            treeNode29});
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Хачапури");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Узел0");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("Грузинская", new System.Windows.Forms.TreeNode[] {
            treeNode31,
            treeNode32});
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("Американская");
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Английская");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("Аргентинская");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("Белорусская");
            System.Windows.Forms.TreeNode treeNode38 = new System.Windows.Forms.TreeNode("Бельгийская");
            System.Windows.Forms.TreeNode treeNode39 = new System.Windows.Forms.TreeNode("Болгарская");
            System.Windows.Forms.TreeNode treeNode40 = new System.Windows.Forms.TreeNode("Венгерская");
            System.Windows.Forms.TreeNode treeNode41 = new System.Windows.Forms.TreeNode("Греческая");
            System.Windows.Forms.TreeNode treeNode42 = new System.Windows.Forms.TreeNode("Грузинская");
            System.Windows.Forms.TreeNode treeNode43 = new System.Windows.Forms.TreeNode("Израильская");
            System.Windows.Forms.TreeNode treeNode44 = new System.Windows.Forms.TreeNode("Индийская");
            System.Windows.Forms.TreeNode treeNode45 = new System.Windows.Forms.TreeNode("Испанская");
            System.Windows.Forms.TreeNode treeNode46 = new System.Windows.Forms.TreeNode("Штрудель");
            System.Windows.Forms.TreeNode treeNode47 = new System.Windows.Forms.TreeNode("Немецкая", new System.Windows.Forms.TreeNode[] {
            treeNode46});
            System.Windows.Forms.TreeNode treeNode48 = new System.Windows.Forms.TreeNode("Португальская");
            System.Windows.Forms.TreeNode treeNode49 = new System.Windows.Forms.TreeNode("Круассаны");
            System.Windows.Forms.TreeNode treeNode50 = new System.Windows.Forms.TreeNode("Французская", new System.Windows.Forms.TreeNode[] {
            treeNode49});
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.главнаяToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipes = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipesCreate = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipesVegeterian = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipesVegan = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipesRawfoodist = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuRecipesAll = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuProfile = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuProfileMyRecipes = new System.Windows.Forms.ToolStripMenuItem();
            this.калькуляторToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.настройкиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuFile = new System.Windows.Forms.ToolStripMenuItem();
            this.создатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.открытьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.сохранитьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.сохранитькакToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.печатьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.предварительныйпросмотрToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuHealthFood = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuHelp = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.MenuHelpAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.RecipeBox = new System.Windows.Forms.RichTextBox();
            this.RecipesTree = new System.Windows.Forms.TreeView();
            this.SearchBox = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip2
            // 
            this.menuStrip2.Font = new System.Drawing.Font("Microsoft YaHei Light", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.главнаяToolStripMenuItem,
            this.MenuFile,
            this.MenuHealthFood,
            this.MenuSettings,
            this.MenuHelp});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(955, 28);
            this.menuStrip2.TabIndex = 7;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // главнаяToolStripMenuItem
            // 
            this.главнаяToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuRecipes,
            this.MenuProfile});
            this.главнаяToolStripMenuItem.Name = "главнаяToolStripMenuItem";
            this.главнаяToolStripMenuItem.Size = new System.Drawing.Size(72, 24);
            this.главнаяToolStripMenuItem.Text = "Главная";
            // 
            // MenuRecipes
            // 
            this.MenuRecipes.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuRecipesCreate,
            this.MenuRecipesVegeterian,
            this.MenuRecipesVegan,
            this.MenuRecipesRawfoodist,
            this.MenuRecipesAll});
            this.MenuRecipes.Name = "MenuRecipes";
            this.MenuRecipes.Size = new System.Drawing.Size(138, 24);
            this.MenuRecipes.Text = "Рецепты";
            // 
            // MenuRecipesCreate
            // 
            this.MenuRecipesCreate.Name = "MenuRecipesCreate";
            this.MenuRecipesCreate.Size = new System.Drawing.Size(180, 24);
            this.MenuRecipesCreate.Text = "Создать";
            this.MenuRecipesCreate.Click += new System.EventHandler(this.MenuRecipesCreate_Click);
            // 
            // MenuRecipesVegeterian
            // 
            this.MenuRecipesVegeterian.Name = "MenuRecipesVegeterian";
            this.MenuRecipesVegeterian.Size = new System.Drawing.Size(180, 24);
            this.MenuRecipesVegeterian.Text = "Вегетарианство";
            this.MenuRecipesVegeterian.Click += new System.EventHandler(this.MenuRecipesVegeterian_Click);
            // 
            // MenuRecipesVegan
            // 
            this.MenuRecipesVegan.Name = "MenuRecipesVegan";
            this.MenuRecipesVegan.Size = new System.Drawing.Size(180, 24);
            this.MenuRecipesVegan.Text = "Веганство";
            this.MenuRecipesVegan.Click += new System.EventHandler(this.MenuRecipesVegan_Click);
            // 
            // MenuRecipesRawfoodist
            // 
            this.MenuRecipesRawfoodist.Name = "MenuRecipesRawfoodist";
            this.MenuRecipesRawfoodist.Size = new System.Drawing.Size(180, 24);
            this.MenuRecipesRawfoodist.Text = "Сыроедение";
            this.MenuRecipesRawfoodist.Click += new System.EventHandler(this.MenuRecipesRawfoodist_Click);
            // 
            // MenuRecipesAll
            // 
            this.MenuRecipesAll.Name = "MenuRecipesAll";
            this.MenuRecipesAll.Size = new System.Drawing.Size(180, 24);
            this.MenuRecipesAll.Text = "Все";
            // 
            // MenuProfile
            // 
            this.MenuProfile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuProfileMyRecipes,
            this.калькуляторToolStripMenuItem,
            this.настройкиToolStripMenuItem});
            this.MenuProfile.Name = "MenuProfile";
            this.MenuProfile.Size = new System.Drawing.Size(138, 24);
            this.MenuProfile.Text = "Профиль";
            this.MenuProfile.Click += new System.EventHandler(this.MenuProfile_Click);
            // 
            // MenuProfileMyRecipes
            // 
            this.MenuProfileMyRecipes.Name = "MenuProfileMyRecipes";
            this.MenuProfileMyRecipes.Size = new System.Drawing.Size(169, 24);
            this.MenuProfileMyRecipes.Text = "Мои рецепты";
            this.MenuProfileMyRecipes.Click += new System.EventHandler(this.MenuProfileMyRecipes_Click);
            // 
            // калькуляторToolStripMenuItem
            // 
            this.калькуляторToolStripMenuItem.Name = "калькуляторToolStripMenuItem";
            this.калькуляторToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.калькуляторToolStripMenuItem.Text = "Калькулятор";
            // 
            // настройкиToolStripMenuItem
            // 
            this.настройкиToolStripMenuItem.Name = "настройкиToolStripMenuItem";
            this.настройкиToolStripMenuItem.Size = new System.Drawing.Size(169, 24);
            this.настройкиToolStripMenuItem.Text = "Настройки";
            // 
            // MenuFile
            // 
            this.MenuFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.создатьToolStripMenuItem,
            this.открытьToolStripMenuItem,
            this.toolStripSeparator,
            this.сохранитьToolStripMenuItem,
            this.сохранитькакToolStripMenuItem,
            this.toolStripSeparator1,
            this.печатьToolStripMenuItem,
            this.предварительныйпросмотрToolStripMenuItem,
            this.toolStripSeparator2});
            this.MenuFile.Name = "MenuFile";
            this.MenuFile.Size = new System.Drawing.Size(55, 24);
            this.MenuFile.Text = "&Файл";
            this.MenuFile.Visible = false;
            // 
            // создатьToolStripMenuItem
            // 
            this.создатьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("создатьToolStripMenuItem.Image")));
            this.создатьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.создатьToolStripMenuItem.Name = "создатьToolStripMenuItem";
            this.создатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
            this.создатьToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.создатьToolStripMenuItem.Text = "&Создать";
            // 
            // открытьToolStripMenuItem
            // 
            this.открытьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("открытьToolStripMenuItem.Image")));
            this.открытьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.открытьToolStripMenuItem.Name = "открытьToolStripMenuItem";
            this.открытьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.открытьToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.открытьToolStripMenuItem.Text = "&Открыть";
            this.открытьToolStripMenuItem.Click += new System.EventHandler(this.открытьToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(263, 6);
            // 
            // сохранитьToolStripMenuItem
            // 
            this.сохранитьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("сохранитьToolStripMenuItem.Image")));
            this.сохранитьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.сохранитьToolStripMenuItem.Name = "сохранитьToolStripMenuItem";
            this.сохранитьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.сохранитьToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.сохранитьToolStripMenuItem.Text = "&Сохранить";
            // 
            // сохранитькакToolStripMenuItem
            // 
            this.сохранитькакToolStripMenuItem.Name = "сохранитькакToolStripMenuItem";
            this.сохранитькакToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.сохранитькакToolStripMenuItem.Text = "Сохранить &как";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(263, 6);
            // 
            // печатьToolStripMenuItem
            // 
            this.печатьToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("печатьToolStripMenuItem.Image")));
            this.печатьToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.печатьToolStripMenuItem.Name = "печатьToolStripMenuItem";
            this.печатьToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.печатьToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.печатьToolStripMenuItem.Text = "&Печать";
            // 
            // предварительныйпросмотрToolStripMenuItem
            // 
            this.предварительныйпросмотрToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("предварительныйпросмотрToolStripMenuItem.Image")));
            this.предварительныйпросмотрToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.предварительныйпросмотрToolStripMenuItem.Name = "предварительныйпросмотрToolStripMenuItem";
            this.предварительныйпросмотрToolStripMenuItem.Size = new System.Drawing.Size(266, 24);
            this.предварительныйпросмотрToolStripMenuItem.Text = "Предварительный про&смотр";
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(263, 6);
            // 
            // MenuHealthFood
            // 
            this.MenuHealthFood.Name = "MenuHealthFood";
            this.MenuHealthFood.Size = new System.Drawing.Size(41, 24);
            this.MenuHealthFood.Text = "ПП";
            this.MenuHealthFood.Click += new System.EventHandler(this.MenuHealthFood_Click);
            // 
            // MenuSettings
            // 
            this.MenuSettings.Name = "MenuSettings";
            this.MenuSettings.Size = new System.Drawing.Size(95, 24);
            this.MenuSettings.Text = "Параметры";
            // 
            // MenuHelp
            // 
            this.MenuHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator5,
            this.MenuHelpAbout});
            this.MenuHelp.Name = "MenuHelp";
            this.MenuHelp.Size = new System.Drawing.Size(74, 24);
            this.MenuHelp.Text = "Спра&вка";
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(173, 6);
            // 
            // MenuHelpAbout
            // 
            this.MenuHelpAbout.Name = "MenuHelpAbout";
            this.MenuHelpAbout.Size = new System.Drawing.Size(176, 24);
            this.MenuHelpAbout.Text = "&О программе...";
            // 
            // RecipeBox
            // 
            this.RecipeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RecipeBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.RecipeBox.Font = new System.Drawing.Font("Microsoft YaHei Light", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipeBox.Location = new System.Drawing.Point(249, 278);
            this.RecipeBox.MaximumSize = new System.Drawing.Size(1800, 580);
            this.RecipeBox.Name = "RecipeBox";
            this.RecipeBox.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.RecipeBox.Size = new System.Drawing.Size(694, 282);
            this.RecipeBox.TabIndex = 9;
            this.RecipeBox.TabStop = false;
            this.RecipeBox.Text = ".";
            this.RecipeBox.Click += new System.EventHandler(this.RecipeBox_Click);
            // 
            // RecipesTree
            // 
            this.RecipesTree.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.RecipesTree.Font = new System.Drawing.Font("Microsoft YaHei Light", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.RecipesTree.Location = new System.Drawing.Point(12, 60);
            this.RecipesTree.MaximumSize = new System.Drawing.Size(550, 1250);
            this.RecipesTree.Name = "RecipesTree";
            treeNode1.Name = "Отбивная";
            treeNode1.Text = "Отбивная";
            treeNode2.Name = "Итальянская";
            treeNode2.Text = "Итальянская";
            treeNode3.Name = "Узел1";
            treeNode3.Text = "Баурсак";
            treeNode4.Name = "Узел2";
            treeNode4.Text = "Казы";
            treeNode5.Name = "Kazakh";
            treeNode5.Text = "Казахская";
            treeNode6.Name = "Китайская";
            treeNode6.Text = "Китайская";
            treeNode7.Name = "Мексиканская";
            treeNode7.Text = "Мексиканская";
            treeNode8.Name = "Poland";
            treeNode8.Text = "Польская";
            treeNode9.Name = "Румынская";
            treeNode9.Text = "Румынская";
            treeNode10.Name = "Узел8";
            treeNode10.Text = "Салат с хреном";
            treeNode11.Name = "Винегрет";
            treeNode11.Text = "Винегрет";
            treeNode12.Name = "Узел11";
            treeNode12.Text = "Драники";
            treeNode13.Name = "Snakes";
            treeNode13.Text = "Закуски";
            treeNode14.Name = "Борщ";
            treeNode14.Text = "Борщ";
            treeNode15.Name = "Окрошка";
            treeNode15.Text = "Окрошка";
            treeNode16.Name = "Щи";
            treeNode16.Text = "Щи";
            treeNode17.Name = "Ботвинья";
            treeNode17.Text = "Ботвинья";
            treeNode18.Name = "Soap";
            treeNode18.Text = "Супы";
            treeNode19.Name = "Вареники";
            treeNode19.Text = "Вареники";
            treeNode20.Name = "Пельмени";
            treeNode20.Text = "Пельмени";
            treeNode21.Name = "Беф строганов";
            treeNode21.Text = "Беф строганов";
            treeNode22.Name = "Биточки";
            treeNode22.Text = "Биточки";
            treeNode23.Name = "Garnish";
            treeNode23.Text = "Гарниры";
            treeNode24.Name = "Хворост";
            treeNode24.Text = "Хворост";
            treeNode25.Name = "Desserts";
            treeNode25.Text = "Десерты";
            treeNode26.Name = "Russian";
            treeNode26.Text = "Русская";
            treeNode27.Name = "Лагман";
            treeNode27.Text = "Лагман";
            treeNode28.Name = "Узбекская";
            treeNode28.Text = "Узбекская";
            treeNode29.Name = "Биточки";
            treeNode29.Text = "Биточки";
            treeNode30.Name = "Украинская";
            treeNode30.Text = "Украинская";
            treeNode31.Name = "Узел10";
            treeNode31.Text = "Хачапури";
            treeNode32.Name = "Узел0";
            treeNode32.Text = "Узел0";
            treeNode33.Name = "Грузинская";
            treeNode33.Text = "Грузинская";
            treeNode34.Name = "Американская";
            treeNode34.Text = "Американская";
            treeNode35.Name = "Узел2";
            treeNode35.Text = "Английская";
            treeNode36.Name = "Аргентинская";
            treeNode36.Text = "Аргентинская";
            treeNode37.Name = "Белорусская";
            treeNode37.Text = "Белорусская";
            treeNode38.Name = "Бельгийская";
            treeNode38.Text = "Бельгийская";
            treeNode39.Name = "Болгарская";
            treeNode39.Text = "Болгарская";
            treeNode40.Name = "Венгерская";
            treeNode40.Text = "Венгерская";
            treeNode41.Name = "Греческая";
            treeNode41.Text = "Греческая";
            treeNode42.Name = "Грузинская";
            treeNode42.Text = "Грузинская";
            treeNode43.Name = "Израильская";
            treeNode43.Text = "Израильская";
            treeNode44.Name = "Индийская";
            treeNode44.Text = "Индийская";
            treeNode45.Name = "Испанская";
            treeNode45.Text = "Испанская";
            treeNode46.Name = "Штрудель";
            treeNode46.Text = "Штрудель";
            treeNode47.Name = "Немецкая";
            treeNode47.Text = "Немецкая";
            treeNode48.Name = "Португальская";
            treeNode48.Text = "Португальская";
            treeNode49.Name = "Круассаны";
            treeNode49.Text = "Круассаны";
            treeNode50.Name = "Французская";
            treeNode50.Text = "Французская";
            this.RecipesTree.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode26,
            treeNode28,
            treeNode30,
            treeNode33,
            treeNode34,
            treeNode35,
            treeNode36,
            treeNode37,
            treeNode38,
            treeNode39,
            treeNode40,
            treeNode41,
            treeNode42,
            treeNode43,
            treeNode44,
            treeNode45,
            treeNode47,
            treeNode48,
            treeNode50});
            this.RecipesTree.Size = new System.Drawing.Size(221, 500);
            this.RecipesTree.TabIndex = 10;
            this.RecipesTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.RecipesTree_AfterSelect);
            // 
            // SearchBox
            // 
            this.SearchBox.Font = new System.Drawing.Font("Microsoft YaHei Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.SearchBox.Location = new System.Drawing.Point(12, 30);
            this.SearchBox.Multiline = true;
            this.SearchBox.Name = "SearchBox";
            this.SearchBox.Size = new System.Drawing.Size(221, 24);
            this.SearchBox.TabIndex = 11;
            this.SearchBox.Text = "Поиск по рецептам..";
            this.SearchBox.WordWrap = false;
            this.SearchBox.Click += new System.EventHandler(this.SearchBox_Click);
            this.SearchBox.TextChanged += new System.EventHandler(this.SearchBox_TextChanged);
            this.SearchBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SearchBox_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.ImageLocation = "https://pp.userapi.com/c621515/v621515459/161d0/yb8wEZY83WA.jpg";
            this.pictureBox1.Location = new System.Drawing.Point(249, 30);
            this.pictureBox1.MaximumSize = new System.Drawing.Size(1800, 500);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(694, 230);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(277, 216);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.DefaultExt = "txt";
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "\"Текстовый файл|*.txt*\"";
            this.openFileDialog1.Title = "Выбор файла";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 570);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SearchBox);
            this.Controls.Add(this.RecipesTree);
            this.Controls.Add(this.RecipeBox);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip2);
            this.Name = "Form1";
            this.Text = "FitStars";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem главнаяToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipes;
        private System.Windows.Forms.ToolStripMenuItem MenuFile;
        private System.Windows.Forms.ToolStripMenuItem создатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem открытьToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem сохранитьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem сохранитькакToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem печатьToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem предварительныйпросмотрToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem MenuHelp;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem MenuHelpAbout;
        private System.Windows.Forms.TreeView RecipesTree;
        private System.Windows.Forms.ToolStripMenuItem MenuProfile;
        private System.Windows.Forms.TextBox SearchBox;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipesCreate;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipesVegeterian;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipesVegan;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipesRawfoodist;
        private System.Windows.Forms.ToolStripMenuItem MenuProfileMyRecipes;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ToolStripMenuItem настройкиToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuHealthFood;
        private System.Windows.Forms.ToolStripMenuItem MenuSettings;
        private System.Windows.Forms.ToolStripMenuItem калькуляторToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MenuRecipesAll;
        private System.Windows.Forms.RichTextBox RecipeBox;
    }
}

