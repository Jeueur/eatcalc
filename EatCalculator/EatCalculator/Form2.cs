﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;

namespace EatCalculator
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Form1 f)
        {
            InitializeComponent();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
        public string reg = "users.xml";
        private void CreateXML(string reg)
        { 
            XmlTextWriter xtw = new XmlTextWriter(reg, Encoding.UTF8);
            xtw.WriteStartDocument();
            xtw.WriteStartElement("users");
            xtw.WriteEndDocument();
            xtw.Close();
        }
        private string MaxID(string reg)
        {
            List<int> idList = new List<int>();
            int id;
            XmlDocument xd = new XmlDocument();
            FileStream fs = new FileStream(reg, FileMode.Open);
            xd.Load(fs);
            XmlNodeList list = xd.GetElementsByTagName("user");
            if (list.Count > 0)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    XmlElement user = (XmlElement)xd.GetElementsByTagName("user")[i];
                    id = Convert.ToInt32(user.GetAttribute("id"));
                    idList.Add(id);
                }
                int result = 0;
                foreach (int j in idList)
                    if (j > result)
                        result = j;
                result++;
                fs.Close();
                return result.ToString();
            }
            else
            {
                fs.Close();
                return "1";
            }
        }
        private void WriteToXMLDocument(string reg, string name, string pwd)
        {
            if (!File.Exists(reg)) CreateXML(reg);
            string id = MaxID(reg);
            XmlDocument xd = new XmlDocument();
            FileStream fs = new FileStream(reg, FileMode.Open);
            xd.Load(fs);

            XmlElement user = xd.CreateElement("user");
            user.SetAttribute("id", id);

            XmlElement login = xd.CreateElement("login");
            XmlElement pass = xd.CreateElement("password");

            XmlText tLogin = xd.CreateTextNode(name);
            XmlText tPass = xd.CreateTextNode(pwd);

            login.AppendChild(tLogin);
            pass.AppendChild(tPass);

            user.AppendChild(login);
            user.AppendChild(pass);

            xd.DocumentElement.AppendChild(user);

            fs.Close();
            xd.Save(reg);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            StreamReader auth = new StreamReader("users.xml", System.Text.Encoding.UTF8);
            auth.ReadLine();
            if (!(LoginBox.Text == "" ) & !(textBox1.Text == ""))
                WriteToXMLDocument(reg, LoginBox.Text, textBox1.Text);
            else
                MessageBox.Show("Введите имя пользователя и пароль");
            /*Form1 newForm = new Form1();
            Form2 authForm = new Form2();
            if (textBox2.Text == "admin")
            {
                this.Hide();
                authForm.Close();
                newForm.Show();
            }
            else
            {
                bool Text = Convert.ToBoolean(textBox2.Text);
                bool login = Text;
                do
                {
                    textBox2.ForeColor = Color.Red;
                }
                while (login == false);
            }*/
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 newForm = new Form1();
            Form2 authForm = new Form2();
            if (!(LoginBox.Text == "") & !(textBox1.Text == ""))
            { 
                this.Hide();
                authForm.Close();
                newForm.Show();
            }
            else
                MessageBox.Show("Введите имя пользователя и пароль");
            /*textBox2.Text = "";
            string newLogin = @"log.txt";
            StreamWriter newLoginCreate = new StreamWriter("log.txt", true, System.Text.Encoding.Default);
            newLoginCreate.WriteLine(textBox2.Text);*/
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            Form1 newForm = new Form1();
            Form2 authForm = new Form2();
            this.Hide();
            authForm.Close();
            newForm.Show();
        }
    }
}
