﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Net;
using EatCalculator;

namespace EatCalculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            /*Profile f = new Profile();
            f.Owner = this;
            f.ShowDialog();*/
        }

        //массив рецептов
        /*public string treeView
        {
            get { return treeView1.Nodes.Find(Name, true); }
            set { textBox1.Text = value; }
        }*/

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        //Form1 form = new Form1();
        //Profile profile = new Profile();

        //public string Node = treeView1.Node.Name;

        private void RecipesTree_AfterSelect(object sender, TreeViewEventArgs e)
        {
            string f3 = this.RecipesTree.SelectedNode.Name;
            try
            {
                RecipeStreamer(f3);
                //this.RecipesTree.Sort();
            }
            catch
            {
                Form3 dialog = new Form3();
                DialogResult result = MessageBox.Show(
                "Рецепт не найден! Нажмите ОК, чтобы создать новый",
                "Ошибка",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button1,
                MessageBoxOptions.DefaultDesktopOnly);

                if (result == DialogResult.Yes)
                    dialog.Show();

                this.TopMost = true;
            }
            //if selection color
        }

        public void twCheck(string kitchen, string type, string dish)
        {
            try
            {
                this.RecipesTree.Nodes.Find(kitchen, true);
            }
            catch
            {
                this.RecipesTree.Nodes.Add(type);
            }
            finally
            {
                try
                {
                    this.RecipesTree.Nodes.Find(type, true);
                }
                catch
                {
                    this.RecipesTree.Nodes.Add(type);
                }
                finally
                {
                    try
                    {
                        this.RecipesTree.Nodes.Find(dish, true);
                    }
                    catch
                    {
                        this.RecipesTree.Nodes.Add(dish);
                    }
                }
           
            }
        }

        public void RecipeStreamer(string name)
        {
            try
            {
                string f1 = ".jpg";
                string f2 = ".txt";
                pictureBox1.Image = Image.FromFile(name + f1);
                RecipeBox.Text = "";
                StreamReader sr = new StreamReader(name + f2, System.Text.Encoding.Default);
                while (!sr.EndOfStream)
                    RecipeBox.Text += sr.ReadLine() + Environment.NewLine;
                sr.Close();
                /*string t = richTextBox1.Text;
                int length = richTextBox1.TextLength;       //at end of text
                //richTextBox1.AppendText(t);                 //добавляет отформатированный текст в конец
                richTextBox1.SelectionStart = length;       //начальная точка
                richTextBox1.SelectionLength = t.Length;*/  //сколько символов форматировать
                                                            //Font.Bold = true;
            }
            catch
            {
                //MessageBox.Show("Рецепта пока нет!");
            }
        }


        private void MenuRecipesCreate_Click(object sender, EventArgs e)
        {
            //Добавление собственного рецепта 
            Profile p = new Profile();
            Form3 dialog = new Form3();
            dialog.Show();
            //При добавлении передавать тип, кухню и режим питания рецепта

            //Проверка на существование и создание узла 
            //задать текст и название узла
        }

        public void CreateRecipe(string kitchen, string type, string name, string pic)
        {
            Form3 dialog = new Form3();
            string Name = name;
            string Type = type;
            string Kitchen = kitchen;
            string Pic = pic;

            try
            {
                twCheck(Kitchen, Type, Name);

                TreeNode node = new TreeNode();
                node.Name = Name;
                node.Text = Name;

                TreeNode types = new TreeNode();
                types.Name = Type;
                types.Text = Type;

                TreeNode kit = new TreeNode();
                node.Name = Kitchen;
                node.Text = Kitchen;

                this.RecipesTree.Nodes.Add(kit);
                this.RecipesTree.Nodes.Add(types);
                //добавление подузлов
                this.RecipesTree.Nodes.Add(node);
            }
            catch
            {
                MessageBox.Show("fewgrsg");
            }
        }

        private void MenuProfile_Click(object sender, EventArgs e)
        {
            //Открытие профиля
            Form1 form = new Form1();
            Profile p = new Profile();
            this.Hide();
            p.Show();
        }

        private void MenuSettings_Click(object sender, EventArgs e)
        {
            //Настройки
            Form1 form = new Form1();
            Profile p = new Profile();
            this.Hide();
            p.Show();
        }

        public void NodeRemove(string name)
        {
            //Просмотр массива рецептов
            //Просмотреть все файлы, исключить по типу 

        }

        private void MenuRecipesVegeterian_Click(object sender, EventArgs e)
        {
            //Удалить все рецепты с мясом и рыбой
        }

        private void MenuRecipesVegan_Click(object sender, EventArgs e)
        {
            //Удалить все рецепты с ингредиентами животного происхождения
            //NodeRemove(meat)
            /*Button cool = new Button();
            cool.Text = "жми";
            this.Controls.Add(cool);
            /*control = (Control)sender;
             * public Control control = null;
            if (control != null)
            {
                control.Parent.Controls.Remove(control);
            }*/
        }

        private void MenuRecipesRawfoodist_Click(object sender, EventArgs e)
        {
            //Удалить все рецепты, включающие термическую обработку
            //NodeRemove()
        }

        private void MenuRecipesAll_Click(object sender, EventArgs e)
        {
            //Отобразить все существующие рецепты
        }


        private List<TreeNode> CurrentNodeMatches = new List<TreeNode>();

        private int LastNodeIndex = 0;

        private string LastSearchText;

        private void SearchNodes(string SearchText, TreeNode StartNode)
        {
            TreeNode node = null;
            while (StartNode != null)
            {
                if (StartNode.Text.ToLower().Contains(SearchText.ToLower()))
                {
                    CurrentNodeMatches.Add(StartNode);
                };
                if (StartNode.Nodes.Count != 0)
                {
                    SearchNodes(SearchText, StartNode.Nodes[0]); //Рекурсивный поиск 
                };
                StartNode = StartNode.NextNode;
            };

        }

        private void SearchBox_TextChanged(object sender, EventArgs e)
        {
            //Поиск по TreeView
            Form1 form = new Form1();
            //if
            //this.treeView1

            string searchText = this.SearchBox.Text;
            if (String.IsNullOrEmpty(searchText))
            {
                return;
            }


            if (LastSearchText != searchText)
            {
                //Поиск
                CurrentNodeMatches.Clear();
                LastSearchText = searchText;
                LastNodeIndex = 0;
                SearchNodes(searchText, RecipesTree.Nodes[0]);
            }

            if (LastNodeIndex >= 0 && CurrentNodeMatches.Count > 0 && LastNodeIndex < CurrentNodeMatches.Count)
            {
                TreeNode selectedNode = CurrentNodeMatches[LastNodeIndex];
                LastNodeIndex++;
                this.RecipesTree.SelectedNode = selectedNode;
                this.RecipesTree.SelectedNode.Expand();
                //this.treeView1.Select();

            }
        }

        private void SearchBox_Click(object sender, EventArgs e)
        {
            SearchBox.Text = "";
        }

        private void SearchBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Выполнить поиск по нажатии ENTER
        }

        private void MenuProfileMyRecipes_Click(object sender, EventArgs e)
        {
            //скрыть все рецепты, кроме добавленных данным пользователем
            //NodeRemove();
        }

        private void RecipeBox_Click(object sender, EventArgs e)
        {
            this.RecipeBox.Text = "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.RecipesTree.Sort();
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*textBox1.Text = "";
                textBox1.Select();        
                string text = textBox1.Text;
                string f = ".txt";
            StreamReader sr = new StreamReader(text + f, System.Text.Encoding.Default);
                while (!sr.EndOfStream)
                    richTextBox1.Text += sr.ReadLine() + Environment.NewLine;
                sr.Close();*/
        }

        public void OpenRecipeByMenu()
        {
            //RecipeStreamer;
        }

        private Button selectButton;

        private void Form1_Resize(object sender, EventArgs e)
        {
            //увеличение элементов при увеличении формы;
            Form1 f = new Form1();
            int sbh = f.SearchBox.Size.Height;
            int sbw = f.SearchBox.Size.Width;

            int rth = f.RecipesTree.Size.Height;
            int rtw = f.RecipesTree.Size.Width;

            int pbh = f.pictureBox1.Size.Height;
            int pbw = f.pictureBox1.Size.Width;

            int rbh = f.RecipeBox.Size.Height;
            int rbw = f.RecipeBox.Size.Width;

            int fh = f.Size.Height;
            int fw = f.Size.Width;
            int i = fh - sbh;
            int j = fw - sbw;

            int hx = f.SearchBox.Location.X;
            int hy = f.SearchBox.Location.Y;

            try
            {
                sbh += fh - i;
                sbw += fw - j;
            }
            catch
            {
                MessageBox.Show("WTF??");
            }
            
        }

        private void MenuHealthFood_Click(object sender, EventArgs e)
        {

        }

        /*public void OpenFileDialogForm()
        {
            openFileDialog1 = new OpenFileDialog();
            selectButton = new Button
            {
                Size = new Size(100, 20),
                Location = new Point(15, 15),
                Text = "Select file"
            };

            SearchBox = new TextBox
            {
                Size = new Size(300, 300),
                Location = new Point(15, 40),
                Multiline = true,
                ScrollBars = ScrollBars.Vertical
            };
            ClientSize = new Size(330, 360);
            Controls.Add(selectButton);
            Controls.Add(SearchBox);
        }*/




    }
}

