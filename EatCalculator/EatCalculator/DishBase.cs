﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EatCalculator.Dish;

namespace EatCalculator
{
    public abstract class DishBase
    {
        protected IDish dishType;

        public DishBase()
        {
            dishType = new NoType();
        }

        public void SetDishType(IDish newDish)
        {
            dishType = newDish;
        }

        public void SendToTW()
        {
            dishType.SendToTW();
        }

    }
}
