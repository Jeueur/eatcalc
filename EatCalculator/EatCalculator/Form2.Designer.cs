﻿namespace EatCalculator
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.LoginBox = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.Auth = new System.Windows.Forms.Button();
            this.Reg = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.textBox1.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(278, 246);
            this.textBox1.MaximumSize = new System.Drawing.Size(558, 68);
            this.textBox1.MaxLength = 50;
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(229, 34);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Пароль";
            // 
            // LoginBox
            // 
            this.LoginBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LoginBox.Font = new System.Drawing.Font("Microsoft YaHei Light", 15.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoginBox.Location = new System.Drawing.Point(278, 198);
            this.LoginBox.MaximumSize = new System.Drawing.Size(558, 68);
            this.LoginBox.MaxLength = 50;
            this.LoginBox.Multiline = true;
            this.LoginBox.Name = "LoginBox";
            this.LoginBox.Size = new System.Drawing.Size(229, 34);
            this.LoginBox.TabIndex = 1;
            this.LoginBox.Text = "Логин";
            this.LoginBox.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            this.LoginBox.Enter += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox3.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.textBox3.Font = new System.Drawing.Font("Microsoft YaHei Light", 28.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox3.Location = new System.Drawing.Point(278, 130);
            this.textBox3.MaximumSize = new System.Drawing.Size(229, 56);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.ReadOnly = true;
            this.textBox3.Size = new System.Drawing.Size(229, 56);
            this.textBox3.TabIndex = 3;
            this.textBox3.Text = "FitStars";
            this.textBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Auth
            // 
            this.Auth.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Auth.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Auth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Auth.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.Auth.Font = new System.Drawing.Font("Microsoft YaHei Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Auth.Location = new System.Drawing.Point(278, 302);
            this.Auth.Name = "Auth";
            this.Auth.Size = new System.Drawing.Size(88, 40);
            this.Auth.TabIndex = 4;
            this.Auth.Text = "Вход";
            this.Auth.UseVisualStyleBackColor = false;
            this.Auth.Click += new System.EventHandler(this.button1_Click);
            // 
            // Reg
            // 
            this.Reg.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Reg.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Reg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Reg.Font = new System.Drawing.Font("Microsoft YaHei Light", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Reg.Location = new System.Drawing.Point(372, 302);
            this.Reg.Name = "Reg";
            this.Reg.Size = new System.Drawing.Size(135, 40);
            this.Reg.TabIndex = 5;
            this.Reg.Text = "Регистрация";
            this.Reg.UseVisualStyleBackColor = false;
            this.Reg.Click += new System.EventHandler(this.button1_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(516, 72);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::EatCalculator.Properties.Resources.рппрввпр;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Reg);
            this.Controls.Add(this.Auth);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.LoginBox);
            this.Controls.Add(this.textBox1);
            this.Name = "Form2";
            this.Text = "EatCalculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox LoginBox;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button Auth;
        private System.Windows.Forms.Button Reg;
        private System.Windows.Forms.Button button1;
    }
}